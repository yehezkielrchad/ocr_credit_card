FROM tensorflow/tensorflow:latest-gpu-py3-jupyter

LABEL maintainer="richard.adiguna@dana.id"

RUN apt-get update && apt-get install -y vim telnet libsm6 libxext6 libxrender-dev

ENV PYTHON_PACKAGES="\
    ipykernel \
    jupyterlab \
    sklearn \
    Pillow \
    pandas \
    opencv-python \
    joblib \
    plotly \
    oss2 \
    pyodps \
"
 
RUN pip install --upgrade pip \
&& pip install --no-cache-dir $PYTHON_PACKAGES

RUN curl -sL https://deb.nodesource.com/setup_13.x | bash - && apt-get install -y nodejs

RUN jupyter labextension install @rahlir/theme-gruvbox \
&& jupyter labextension install jupyterlab-plotly --no-build \
&& jupyter labextension install plotlywidget --no-build \
&& jupyter lab build
