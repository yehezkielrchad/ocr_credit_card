s script read configuration from your runDocker.conf files.
# Change the default path if you wish.
# ======================================================
. ~/runDocker.conf

# ======================================================
# Don't change anything below this line
# ======================================================
# Sanity check
IS_SANE="0"
if [ $# -eq 0 ]; then
        printf "Nothing to run. Please run: <runDocker.sh help> for.. help\n"
        exit
fi


# Call for help
if [ $1 == "help" ]; then
        clear
        printf "To use:\n"
        printf "./runDocker.sh <tensor [token <docker_id>]| r | mysql  | list | kill [docker_id]>\n\n"
        printf "tensor: run tensorflow docker.\n"
        printf "r: run R studio docker.\n"
        printf "mysql: run MySQL docker.\n"
        printf "list: list all running docker.\n"
        printf "kill: shut down a running docker. Follows this argument by 'about-to-die' docker ID.  Use list argumen
t to check the docker ID.\n\n"
        printf "Example:\n"
        printf "runDocker.sh tensor\n"
        exit
fi

# Run script
if [ $1 == "tensor" ]; then
        if [ $2 == "token" ]; then
                if [ $# -gt 2 ]; then
                        # Get token
                        docker exec $3 jupyter notebook list
                        IS_SANE="1"
                        exit
                else
                        printf "Please provide docker ID\n"
                        exit
                fi
        fi

        if [ $2 == "bash" ]; then
                printf "======================================================\n"
                printf "Don't forget to install your library and additional tools manually. Just use pip install [LIB]\n"
                printf "To run your custom notebook use: jupyter notebook --ip=0.0.0.0 --port=[YOUR_PORT] --allow-root\n"
                printf "======================================================\n"
                docker run --gpus all -v $HOST_DIR:$J_DOCKER_DIR  -p $J_PORT:$J_PORT_DOCKER -it --rm $BASE_IMAGE bash
                exit
        fi

        clear
        printf "======================================================\n"
        printf "Pressing ENTER will immediately install everything you put in APP variables.\n"
        printf "The docker will continue forever if God forbid.  Please kill the docker when the job's done.\n"
        printf "Your TOKEN will be provided after the docker run properly.\n"
        printf "Your Jupiter notebook: http://149.129.249.82:%s/\n" $J_PORT
        printf "Don't forget to kill your docker after use.\n"
        printf "To run you custom tensorflow docker use: runDocker tensor bash\n"
        printf "======================================================\n"
        read -s -p "Press ENTER to run $1, or ctrl-C to cancel"

        # Running the docker
        DOCKER_ID="$(docker run -v $HOST_DIR:$J_DOCKER_DIR  -d --gpus all -p $J_PORT:8888 --rm tensorflow/tensorflow:latest-gpu-py3-jupyter 2>&1)"

        # Install apps
        docker container exec $DOCKER_ID apt install $APP
        docker container exec $DOCKER_ID pip install --upgrade pip
        docker container exec $DOCKER_ID pip install $LIB_INSTALL
        #docker container exec $DOCKER_ID cd /root/.jupyter
        #docker container exec $DOCKER_ID jupyter notebook --generate-config
        #docker container exec $DOCKER_ID echo "c.NotebookApp.port = $J_PORT_DOCKER" >> /root/.jupyter/jupyter_notebook_config.py
        #docker container exec $DOCKER_ID less /root/.jupyter/jupyter_notebook_config.py

        # Get token
        TENSOR_TOKEN="$(docker exec $DOCKER_ID jupyter notebook list 2>&1)"
        printf "Your token: %s\n" ${TENSOR_TOKEN:54:48}

        # Report sanity
        IS_SANE="1"
fi
